﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Bob";
            int jersey = 83;
            int score = 2;
            HockeyPlayer player = new HockeyPlayer(name, jersey, score);
            Console.WriteLine("{0}  {1}  {2}", player.Name, player.Jersey, player.Goals);


        }
    }
}
