﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class HockeyPlayer
    {
        private string _playerName;
        private int _jerseyNumber;
        private int _goalsScored;


        public HockeyPlayer(string name, int number, int goals)
        {
            _playerName = name;
            _jerseyNumber = number;
            _goalsScored = goals;
        }

        public string Name
        {
            get { return _playerName; }
        }
        public int Jersey
        {
            get { return _jerseyNumber; }
        }
        public int Goals
        {
           get { return _goalsScored; }
        }

    }
}
