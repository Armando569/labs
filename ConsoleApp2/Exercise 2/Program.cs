﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    class Program
    {
        static void Main(string[] args)
        { 
            ClassifiedAd ad = new ClassifiedAd();
            ad.Category = "Used Cars";
            ad.NumberOfWords = 5;
            Console.WriteLine("The Ad for {0} with {1} words will be {2}", ad.Category, ad.NumberOfWords, ad.PricePerWord.ToString("C"));

        }
    }
}
