﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    class ClassifiedAd
    {
        private string _category;
        private int _numberOfWords;
        private double _price;

        //public ClassifiedAd(string category, int numOfWords, double price)
        //{
        //    _category = category;
        //    _numberOfWords = numOfWords;
        //    _price = price;
        //}

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        public int NumberOfWords
        {
            get { return _numberOfWords; }
            set { _numberOfWords = value;
                CalcPrice();
            }
        }
        public double PricePerWord
        {
            get { return _price; }
        }

        private void CalcPrice()
        {
            _price = 0.09 * NumberOfWords;
        }
    }
}
